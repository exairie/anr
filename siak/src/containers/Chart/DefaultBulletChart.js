import React from "react";
import Highcharts from "highcharts";
import More from "highcharts/highcharts-more";
import HighchartsReact from "highcharts-react-official";
import Bullet from "highcharts/modules/bullet";
import { Col, Row } from "reactstrap";

More(Highcharts);
Bullet(Highcharts);

const defaultChartOptions = {
  chart: {
    inverted: true,
    marginLeft: 135,
    type: "bullet"
  },
  title: {
    text: null
  },
  legend: {
    enabled: false
  },
  yAxis: {
    gridLineWidth: 0,
    plotBands: [
      {
        from: 0,
        to: 2,
        color: "#666"
      },
      {
        from: 2,
        to: 3,
        color: "#999"
      },
      {
        from: 3,
        to: 4,
        color: "#bbb"
      }
    ],
    title: {
      text: "IPK"
    }
  },
  plotOptions: {
    series: {
      pointPadding: 0.25,
      borderWidth: 0,
      color: "#000",
      targetOptions: {
        width: "200%"
      }
    }
  },
  credits: {
    enabled: false
  },
  exporting: {
    enabled: false
  }
};

export default function DefaultBulletChart(props) {
  const {
    chartExtraOptions,
    y,
    target,
    chartTitle,
    chartSubtitle,
    semester
  } = props;
  return (
    <div className="animated fadeIn">
      <Row>
        <Col>
          <HighchartsReact
            highcharts={Highcharts}
            options={{
              ...defaultChartOptions,
              ...chartExtraOptions,
              xAxis: [
                {
                  categories: semester,
                  crosshair: true
                }
              ],
              title: {
                text: chartTitle
              },
              subtitle: {
                text: chartSubtitle
              },
              series: {
                data: [
                  {
                    y,
                    target
                  }
                ]
              }
            }}
          />
        </Col>
      </Row>
    </div>
  );
}
