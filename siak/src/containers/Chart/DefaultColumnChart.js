import React, { useState } from "react";
import Highcharts from "highcharts";
import Exporting from "highcharts/modules/exporting";
import ExportingData from "highcharts/modules/export-data";
import More from "highcharts/highcharts-more";
import Theme from "highcharts/themes/gray";
import HighchartsReact from "highcharts-react-official";
import { Col, Row } from "reactstrap";

Exporting(Highcharts);
ExportingData(Highcharts);
Theme(Highcharts);

const defaultChartOptions = {
  chart: {
    zoomType: "xy",
  },
  exporting: {
    printMaxWidth: 1600,
    scale: 10,
    width: 2500,
  },

  title: {
    text: "Average Monthly Temperature and Rainfall in Tokyo",
  },
  subtitle: {
    text: "Source: WorldClimate.com",
  },
  xAxis: [
    {
      categories: [
        "Jan",
        "Feb",
        "Mar",
        "Apr",
        "May",
        "Jun",
        "Jul",
        "Aug",
        "Sep",
        "Oct",
        "Nov",
        "Dec",
      ],
      crosshair: true,
    },
  ],
  yAxis: [
    {
      // Primary yAxis
      labels: {
        format: "{value:0.1f}",
        style: {
          color: Highcharts.getOptions().colors[1],
        },
      },
      title: {
        text: "IPK (0-4)",
        style: {
          color: Highcharts.getOptions().colors[1],
        },
      },
    },
    {
      // Secondary yAxis
      title: {
        text: "Semester",
        style: {
          color: Highcharts.getOptions().colors[0],
        },
      },
      labels: {
        format: "{value}",
        style: {
          color: Highcharts.getOptions().colors[0],
        },
      },
      opposite: false,
    },
  ],
  tooltip: {
    shared: true,
    formatter: function () {
      var points = this.points;
      var pointsLength = points.length;
      var tooltipMarkup = pointsLength
        ? "<b>Semester " + points[0].key + "</b><br/>"
        : "";
      var index;
      var y_value_kwh;

      for (index = 0; index < pointsLength; index += 1) {
        y_value_kwh = (points[index].y / 1000).toFixed(2);

        tooltipMarkup +=
          '<span style="color:' +
          points[index].series.color +
          '">\u25CF</span> ' +
          points[index].series.name +
          ": <b>" +
          points[index].y.toFixed(2) +
          " </b><br/>";
      }

      return tooltipMarkup;
    },
  },

  legend: {
    layout: "horizontal",
    align: "left",
    // x: 120,
    verticalAlign: "bottom",
    // y: 100,
    floating: false,
    backgroundColor:
      Highcharts.defaultOptions.legend.backgroundColor || // theme
      "rgba(255,255,255,0.25)",
  },
  series: [
    {
      name: "Rainfall",
      type: "column",
      yAxis: 1,
      data: [
        49.9,
        71.5,
        106.4,
        129.2,
        144.0,
        176.0,
        135.6,
        148.5,
        216.4,
        194.1,
        95.6,
        54.4,
      ],
      tooltip: {
        valueSuffix: " mm",
      },
    },
    {
      name: "Temperature",
      type: "spline",
      data: [
        7.0,
        6.9,
        9.5,
        14.5,
        18.2,
        21.5,
        25.2,
        26.5,
        23.3,
        18.3,
        13.9,
        9.6,
      ],
      tooltip: {
        valueSuffix: "°C",
      },
    },
  ],
};

export default function DefaultColumnChart(props) {
  const [shown, setShown] = useState(false);
  const {
    chartExtraOptions,
    series,
    chartTitle,
    chartSubtitle,
    semester,
  } = props;
  setTimeout(() => {
    setShown(true);
  }, 1000);
  if (!shown) return <div />;
  return (
    <div style={{ width: "100%" }}>
      <HighchartsReact
        highcharts={Highcharts}
        options={{
          ...defaultChartOptions,
          ...chartExtraOptions,
          xAxis: [
            {
              categories: semester,
              crosshair: true,
            },
          ],
          title: {
            text: chartTitle,
          },
          subtitle: {
            text: chartSubtitle,
          },
          series,
        }}
      />
    </div>
  );
}
