import React from "react";
import Highcharts from "highcharts";
import Drilldown from "highcharts/modules/drilldown";
import Exporting from "highcharts/modules/exporting";
import ExportData from "highcharts/modules/export-data";
import More from "highcharts/highcharts-more";
import HighchartsReact from "highcharts-react-official";
import { Col, Row } from "reactstrap";

Exporting(Highcharts);
ExportData(Highcharts);

const defaultChartOptions = {
  chart: {
    plotBackgroundColor: null,
    plotBorderWidth: null,
    plotShadow: false,
    type: "pie",
  },
  title: {
    text: "Browser market shares in January, 2018",
  },
  tooltip: {
    pointFormat: "{series.name}: <b>{point.percentage:.1f}%</b>",
  },
  plotOptions: {
    pie: {
      allowPointSelect: true,
      cursor: "pointer",
      dataLabels: {
        enabled: true,
        format: "<b>{point.name}</b>: {point.y}",
      },
    },
  },
  exporting: {
    csv: {
      decimalPoint: ".",
      dateFormat: "%Y-%m-%d %H:%M:%S",
    },
    buttons: {
      contextButton: {
        menuItems: ["printChart", "downloadPNG", "downloadJPEG", "downloadCSV"],
      },
    },
  },
  series: [
    {
      name: "Brands",
      colorByPoint: true,
      data: [
        {
          name: "Chrome",
          y: 61.41,
          sliced: true,
          selected: true,
        },
        {
          name: "Internet Explorer",
          y: 11.84,
        },
        {
          name: "Firefox",
          y: 10.85,
        },
        {
          name: "Edge",
          y: 4.67,
        },
        {
          name: "Safari",
          y: 4.18,
        },
        {
          name: "Sogou Explorer",
          y: 1.64,
        },
        {
          name: "Opera",
          y: 1.6,
        },
        {
          name: "QQ",
          y: 1.2,
        },
        {
          name: "Other",
          y: 2.61,
        },
      ],
    },
  ],
};

More(Highcharts);
Drilldown(Highcharts);
Exporting(Highcharts);
ExportData(Highcharts);

export default function DefaultPieChart(props) {
  const {
    chartExtraOptions,
    series,
    chartTitle,
    chartSubtitle,
    data,
    drilldown,
  } = props;
  console.log(data, drilldown);
  return (
    <div className="animated fadeIn">
      <Row>
        <Col>
          <HighchartsReact
            highcharts={Highcharts}
            options={{
              ...defaultChartOptions,
              ...chartExtraOptions,
              title: {
                text: chartTitle,
              },
              series: [
                {
                  name: "Jumlah Mahasiswa",
                  colorByPoint: true,
                  data,
                },
              ],
              drilldown: {
                ...drilldown,
              },
            }}
          />
        </Col>
      </Row>
    </div>
  );
}
