import { ApolloClient } from "apollo-client";
import { InMemoryCache } from "apollo-cache-inmemory";
import { HttpLink } from "apollo-link-http";
import { WebSocketLink } from "apollo-link-ws";
import { split } from "apollo-link";
import { getMainDefinition } from "apollo-utilities";

const wsLink = new WebSocketLink({
  uri: "ws://datditdut.com:9090/graphql",
  options: {
    reconnect: true
  }
});
const httpLink = new HttpLink({
  uri: "http://datditdut.com:9090/graphql"
});

const Link = split(
  ({ query }) => {
    const definition = getMainDefinition(query);
    return (
      definition.kind === "OperationDefinition" &&
      definition.operation === "subscription"
    );
  },
  wsLink,
  httpLink
);

const Apollo = new ApolloClient({
  link: Link,
  cache: new InMemoryCache()
});

export default Apollo;
