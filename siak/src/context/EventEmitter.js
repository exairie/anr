import EventEmitter from "events";

export const EVENT_ENG_STATUS = "evtengstat";
export const EVENT_TEMP = "evtengtemp";

const Events = new EventEmitter();

export function emitEngineStatus(statuses) {
  Events.emit(EVENT_ENG_STATUS, statuses);
}

export default Events;
