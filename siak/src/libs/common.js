export function installExtensions() {
  Array.prototype.findValue = function(parameter) {
    var value = "";

    for (var i of this) {
      if (typeof i !== "object") continue;
      if (typeof i.parameter !== "undefined" && typeof i.value != "undefined") {
        if (i.parameter === parameter.toLowerCase()) {
          value = i.value;
          break;
        }
      }
    }
    return value;
  };
}
