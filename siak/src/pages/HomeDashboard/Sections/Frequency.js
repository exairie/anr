import React from "react";

export default function Frequency(props) {
  const { data } = props;
  const freq = data ? data.findValue("freq") : 0;
  return (
    <div className="frequency">
      <h4>Frekuensi</h4>
      <h3 className="text-right">{freq} Hz</h3>
    </div>
  );
}
