import React, { useState, useEffect } from "react";
import { Card } from "reactstrap";
import { useMutation } from "react-apollo";
import gql from "graphql-tag";

const sendCommandQuery = gql`
  mutation command($mac: String, $command: String) {
    pressButton(code: $command, mac: $mac) {
      result
    }
  }
`;

export default function LEDControl(props) {
  const { currentMac, control } = props;
  const [mutate] = useMutation(sendCommandQuery);

  /** ON/OFF */
  const sendCommand = cmd => {
    mutate({
      variables: {
        mac: currentMac,
        command: `${control}_${cmd}`
      }
    }).then(c => {
      console.log(c);
    });
  };

  const [buttonOn, setButtonOn] = useState(false);
  useEffect(() => {
    const { data, filter } = props;
    if (!data) return;
    if (data.findValue(filter) == true) {
      setButtonOn(true);
    } else {
      setButtonOn(false);
    }
    return () => {};
  }, [props]);
  return (
    <Card className="ledmonitor">
      <div className="row">
        <div className="col-md-12">
          <h4>LED Controls</h4>
          <div
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              flexDirection: "row"
            }}
          >
            <h3 style={{ padding: "8px 20px", margin: 0 }}>LED 1</h3>
            <div
              className="led-btn on"
              style={{ cursor: "pointer", flex: 1 }}
              onClick={e => {
                sendCommand("ON");
              }}
            >
              ON
            </div>
            <div
              className="led-btn off"
              style={{ cursor: "pointer", flex: 1 }}
              onClick={e => {
                sendCommand("OFF");
              }}
            >
              OFF
            </div>
          </div>
          <div className="row">
            <div className="col-md-6 text-center ledpos">
              <h5 style={{ margin: 0 }}>LED 2 Status</h5>
            </div>
            <div className="col-md-6 text-center ledpos">
              <div className={"led-circle " + (buttonOn ? "on" : "off")}></div>{" "}
              <span>{buttonOn ? "ON" : "OFF"}</span>
            </div>
          </div>
        </div>
      </div>
    </Card>
  );
}
