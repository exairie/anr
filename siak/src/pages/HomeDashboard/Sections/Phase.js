import React, { useEffect, useState, useRef } from "react";
import Highcharts from "highcharts";
import { Card } from "reactstrap";
import HighchartsReact from "highcharts-react-official";
import HighchartGauge from "highcharts/modules/solid-gauge";
import More from "highcharts/highcharts-more";

More(Highcharts);
HighchartGauge(Highcharts);

const GAUGEVOLTAGE = {
  chart: {
    type: "solidgauge",
    height: 250,
    title: "Voltage"
  },

  title: {
    text: "Voltage (V)"
  },

  pane: {
    center: ["50%", "55%"],
    size: "100%",
    startAngle: -90,
    endAngle: 90,
    background: {
      backgroundColor:
        Highcharts.defaultOptions.legend.backgroundColor || "#EEE",
      innerRadius: "60%",
      outerRadius: "100%",
      shape: "arc"
    }
  },

  exporting: {
    enabled: false
  },

  tooltip: {
    enabled: false
  },

  // the value axis
  yAxis: {
    stops: [
      [0.1, "#55BF3B"], // green
      [0.5, "#DDDF0D"], // yellow
      [0.8, "#DF5353"] // red
    ],
    lineWidth: 0,
    tickWidth: 0,
    min: 0,
    max: 400,
    tickInterval: parseInt(400 / 1000),
    minorTickInterval: null,
    tickAmount: 2,
    title: {
      y: -70
    },
    labels: {
      y: 16
    }
  },

  plotOptions: {
    solidgauge: {
      dataLabels: {
        y: 5,
        borderWidth: 0,
        useHTML: true
      }
    }
  },

  series: [
    {
      dataLabels: {
        format:
          '<div style="text-align:center">' +
          '<span style="font-size:25px">{y}</span><br/>' +
          '<span style="font-size:12px;opacity:0.4">Volt</span>' +
          "</div>"
      },
      tooltip: {
        valueSuffix: " Volt"
      },
      name: "Ampere",
      data: [20],
      yAxis: 0
    }
  ]
};
const GAUGECURRENT = {
  chart: {
    type: "solidgauge",
    height: 250,
    title: "Voltage"
  },

  title: {
    text: "Current (A)"
  },

  pane: {
    center: ["50%", "55%"],
    size: "100%",
    startAngle: -90,
    endAngle: 90,
    background: {
      backgroundColor:
        Highcharts.defaultOptions.legend.backgroundColor || "#EEE",
      innerRadius: "60%",
      outerRadius: "100%",
      shape: "arc"
    }
  },

  exporting: {
    enabled: false
  },

  tooltip: {
    enabled: false
  },

  // the value axis
  yAxis: {
    stops: [
      [0.1, "#55BF3B"], // green
      [0.5, "#DDDF0D"], // yellow
      [0.8, "#DF5353"] // red
    ],
    lineWidth: 0,
    tickWidth: 0,
    min: 0,
    max: 60,
    tickInterval: parseInt(60 / 1000),
    minorTickInterval: null,
    tickAmount: 2,
    title: {
      y: -70
    },
    labels: {
      y: 16
    }
  },

  plotOptions: {
    solidgauge: {
      dataLabels: {
        y: 5,
        borderWidth: 0,
        useHTML: true
      }
    }
  },

  series: [
    {
      dataLabels: {
        format:
          '<div style="text-align:center">' +
          '<span style="font-size:25px">{y}</span><br/>' +
          '<span style="font-size:12px;opacity:0.4">Amp</span>' +
          "</div>"
      },
      tooltip: {
        valueSuffix: " Amp"
      },
      name: "Ampere",
      data: [20],
      yAxis: 0
    }
  ]
};
const GAUGEPOWER = {
  chart: {
    type: "solidgauge",
    height: 250,
    title: "Voltage"
  },

  title: {
    text: "Power (KW)"
  },

  pane: {
    center: ["50%", "55%"],
    size: "100%",
    startAngle: -90,
    endAngle: 90,
    background: {
      backgroundColor:
        Highcharts.defaultOptions.legend.backgroundColor || "#EEE",
      innerRadius: "60%",
      outerRadius: "100%",
      shape: "arc"
    }
  },

  exporting: {
    enabled: false
  },

  tooltip: {
    enabled: false
  },

  // the value axis
  yAxis: {
    stops: [
      [0.1, "#55BF3B"], // green
      [0.5, "#DDDF0D"], // yellow
      [0.8, "#DF5353"] // red
    ],
    lineWidth: 0,
    tickWidth: 0,
    tickInterval: parseInt(
      ((GAUGEVOLTAGE.yAxis.max / 1000) * GAUGECURRENT.yAxis.max) / 1000 / 1000
    ),
    min: 0,
    max: (GAUGEVOLTAGE.yAxis.max * GAUGECURRENT.yAxis.max) / 1000,
    minorTickInterval: null,
    tickAmount: 2,
    title: {
      y: -70
    },
    labels: {
      y: 16
    }
  },

  plotOptions: {
    solidgauge: {
      dataLabels: {
        y: 5,
        borderWidth: 0,
        useHTML: true
      }
    }
  },

  series: [
    {
      dataLabels: {
        format:
          '<div style="text-align:center">' +
          '<span style="font-size:25px">{y}</span><br/>' +
          '<span style="font-size:12px;opacity:0.4">KW</span>' +
          "</div>"
      },
      tooltip: {
        valueSuffix: " KW"
      },
      name: "Ampere",
      data: [20],
      yAxis: 0
    }
  ]
};
const GAUGEVOLTAGETO = {
  chart: {
    type: "solidgauge",
    height: 250,
    title: "Voltage"
  },

  title: {
    text: "Voltage (V)"
  },

  pane: {
    center: ["50%", "55%"],
    size: "100%",
    startAngle: -90,
    endAngle: 90,
    background: {
      backgroundColor:
        Highcharts.defaultOptions.legend.backgroundColor || "#EEE",
      innerRadius: "60%",
      outerRadius: "100%",
      shape: "arc"
    }
  },

  exporting: {
    enabled: false
  },

  tooltip: {
    enabled: false
  },

  // the value axis
  yAxis: {
    stops: [
      [0.1, "#55BF3B"], // green
      [0.5, "#DDDF0D"], // yellow
      [0.8, "#DF5353"] // red
    ],
    lineWidth: 0,
    tickWidth: 0,
    min: 0,
    max: 1200,
    tickInterval: parseInt(1200 / 1000),
    minorTickInterval: true,
    tickAmount: 1,
    title: {
      y: -70
    },
    labels: {
      y: 16
    }
  },

  plotOptions: {
    solidgauge: {
      dataLabels: {
        y: 5,
        borderWidth: 0,
        useHTML: true
      }
    }
  },

  series: [
    {
      dataLabels: {
        format:
          '<div style="text-align:center">' +
          '<span style="font-size:25px">{y}</span><br/>' +
          '<span style="font-size:12px;opacity:0.4">Volt</span>' +
          "</div>"
      },
      tooltip: {
        valueSuffix: " Volt"
      },
      name: "Ampere",
      data: [20],
      yAxis: 0
    }
  ]
};
const GAUGECURRENTTO = {
  chart: {
    type: "solidgauge",
    height: 250,
    title: "Voltage"
  },

  title: {
    text: "Current (A)"
  },

  pane: {
    center: ["50%", "55%"],
    size: "100%",
    startAngle: -90,
    endAngle: 90,
    background: {
      backgroundColor:
        Highcharts.defaultOptions.legend.backgroundColor || "#EEE",
      innerRadius: "60%",
      outerRadius: "100%",
      shape: "arc"
    }
  },

  exporting: {
    enabled: false
  },

  tooltip: {
    enabled: false
  },

  // the value axis
  yAxis: {
    stops: [
      [0.1, "#55BF3B"], // green
      [0.5, "#DDDF0D"], // yellow
      [0.8, "#DF5353"] // red
    ],
    lineWidth: 0,
    tickWidth: 0,
    min: 0,
    max: 180,
    tickInterval: parseInt(240 / 1000),
    minorTickInterval: null,
    tickAmount: 2,
    title: {
      y: -70
      // x: -10
    },
    labels: {
      y: 16
    }
  },

  plotOptions: {
    solidgauge: {
      dataLabels: {
        y: 5,
        borderWidth: 0,
        useHTML: true
      }
    }
  },

  series: [
    {
      dataLabels: {
        format:
          '<div style="text-align:center">' +
          '<span style="font-size:25px">{y}</span><br/>' +
          '<span style="font-size:12px;opacity:0.4">Amp</span>' +
          "</div>"
      },
      tooltip: {
        valueSuffix: " Amp"
      },
      name: "Ampere",
      data: [20],
      yAxis: 0
    }
  ]
};
const GAUGEPOWERTO = {
  chart: {
    type: "solidgauge",
    height: 250,
    title: "Voltage"
  },

  title: {
    text: "Power (KW)"
  },

  pane: {
    center: ["50%", "55%"],
    size: "100%",
    startAngle: -90,
    endAngle: 90,
    background: {
      backgroundColor:
        Highcharts.defaultOptions.legend.backgroundColor || "#EEE",
      innerRadius: "60%",
      outerRadius: "100%",
      shape: "arc"
    }
  },

  exporting: {
    enabled: false
  },

  tooltip: {
    enabled: false
  },

  // the value axis
  yAxis: {
    stops: [
      [0.1, "#55BF3B"], // green
      [0.5, "#DDDF0D"], // yellow
      [0.8, "#DF5353"] // red
    ],
    lineWidth: 0,
    tickWidth: 0,
    tickInterval: parseInt(
      ((GAUGEVOLTAGETO.yAxis.max / 1000) * (GAUGECURRENTTO.yAxis.max / 1000)) /
        1000
    ),
    min: 0,
    max: (GAUGEVOLTAGETO.yAxis.max * GAUGECURRENTTO.yAxis.max) / 1000,
    minorTickInterval: null,
    tickAmount: 2,
    title: {
      y: -70
    },
    labels: {
      y: 16
    }
  },

  plotOptions: {
    solidgauge: {
      dataLabels: {
        y: 5,
        borderWidth: 0,
        useHTML: true
      }
    }
  },

  series: [
    {
      dataLabels: {
        format:
          '<div style="text-align:center">' +
          '<span style="font-size:25px">{y}</span><br/>' +
          '<span style="font-size:12px;opacity:0.4">KW</span>' +
          "</div>"
      },
      tooltip: {
        valueSuffix: " KW"
      },
      name: "Ampere",
      data: [20],
      yAxis: 0
    }
  ]
};
export default function Phase(props) {
  const { title, data, filter } = props;
  const chartVoltage = useRef(null);
  const chartCurrent = useRef(null);
  const chartPower = useRef(null);
  useEffect(() => {
    const { data, filter } = props;
    if (!data) return;
    const voltage = Number(data.findValue("volt_" + filter));
    const current = Number(data.findValue("curr_" + filter));
    const power = (voltage * current) / 1000;
    console.log(`Phase V:${voltage} A:${current} P:${power}`);
    if (chartVoltage.current) {
      chartVoltage.current.chart.series[0].setData([voltage]);
    }
    if (chartCurrent.current) {
      chartCurrent.current.chart.series[0].setData([current]);
    }
    if (chartPower.current) {
      chartPower.current.chart.series[0].setData([power]);
    }
    return () => {};
  }, [props]);
  return (
    <div className="row">
      <div className="col-md-12">
        <Card>
          <h2 className="text-center" style={{ padding: "8px 10px" }}>
            {title}
          </h2>
          <div className="row">
            <div className="col-md-12" style={{ height: "250px" }}>
              <HighchartsReact
                ref={chartVoltage}
                highcharts={Highcharts}
                options={filter !== "tot" ? GAUGEVOLTAGE : GAUGEVOLTAGETO}
              />
            </div>
          </div>
          <div className="row">
            <div className="col-md-12" style={{ height: "250px" }}>
              <HighchartsReact
                ref={chartCurrent}
                highcharts={Highcharts}
                options={filter !== "tot" ? GAUGECURRENT : GAUGECURRENTTO}
              />
            </div>
          </div>
          <div className="row">
            <div className="col-md-12" style={{ height: "250px" }}>
              <HighchartsReact
                ref={chartPower}
                highcharts={Highcharts}
                options={filter !== "tot" ? GAUGEPOWER : GAUGEPOWERTO}
              />
            </div>
          </div>
        </Card>
      </div>
    </div>
  );
}
