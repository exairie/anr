import React, { useState, useEffect } from "react";
import { Row, Col, Card, CardHeader, CardBody } from "reactstrap";
import DefaultColumnChart from "../../containers/Chart/DefaultColumnChart";

const ListProdi = ["P. TIK", "P. Elektronika", "P. Elektro", "P. Tata Boga"];
const ListFakultas = [
  {
    fak: "Fakultas Ilmu Pendidikan",
    prod: [
      "Bimbingan Dan Konseling",
      "Manajeman Pendidikan",
      "Pendidikan Guru PAUD",
      "Pendidikan Guru Sekolah Dasar",
      "Pendidikan Luar Biasa",
      "Pendidikan Luar Sekolah",
      "Teknologi Pendidikan",
    ],
  },
  {
    fak: "Fakultas Ilmu Sosial",
    prod: [
      "Ilmu Agama Islam",
      "Pendidikan Geografi",
      "Pendidikan IPS",
      "PPKN",
      "Pendidikan Sejarah",
      "Pendidikan Sosiologi",
      "Sosiologi Ilmu Komunikasi",
    ],
  },
  {
    fak: "Fakultas Teknik",
    prod: [
      "Pendidikan Kesejahteraan Keluarga",
      "Pendidikan Tata Boga",
      "Pendidikan Tata Busana",
      "Pendidikan Tata Rias",
      "Pendidikan Teknik Bangunan",
      "Pendidikan Teknik Elektro",
      "Pendidikan Teknik Elektronika",
      "Pendidikan Teknik Informatika dan Komputer",
      "Pendidikan Teknik Mesin",
      "Rekayasa Keselamatan Kebakaran Teknik Mesin",
    ],
  },
  {
    fak: "Fakultas MIPA",
    prod: [
      "Ilmu Komputer",
      "Kimia",
      "Pendidikan Matematika",
      "Pendidikan Fisika",
      "Pendidikan Biologi",
      "Pendidikan Kimia",
    ],
  },
];

export default function IPK(props) {
  const [semester, setSemester] = useState(106);
  const [randomNumber, setRandomNumber] = useState(10);
  const [smtStart, setSmtStart] = useState(106);
  const [smtEnd, setSmtEnd] = useState(115);
  const [angkatan, setAngkatan] = useState(-1);
  const [selectedFakultas, setSelectedFakultas] = useState("null");
  useEffect(() => {
    setRandomNumber(smtEnd - smtStart);
    return () => {
      setRandomNumber(10);
    };
  }, [smtStart, smtEnd]);
  const randomSemester = () => {
    return Array.apply(null, Array(smtEnd - smtStart + 1)).map(
      (x, i) => smtStart + i
    );
  };
  const randomDataFakultas = () => {
    return ListFakultas.map((x) => ({
      name: x.fak,
      type: "line",
      data: randomSemester().map((x) => 3 + Math.random()),
    }));
  };
  const randomDataUniversitas = () => {
    return [
      {
        name: "IPK",
        type: "line",
        data: randomSemester().map((x) => 3 + Math.random()),
      },
    ];
  };
  const randomDataProdi = () => {
    const search = ListFakultas.filter((x) => x.fak == selectedFakultas);
    if (search.length < 1) return [];
    return search[0].prod.map((x) => ({
      name: x,
      type: "line",
      data: randomSemester().map((x) => 3 + Math.random()),
    }));
  };
  return (
    <div className="animated fadeIn">
      <Card>
        <CardHeader>
          <Row>
            <Col>
              <h4>IPK Rata Rata per Fakultas</h4>
            </Col>
            <Col md="2">
              <div className="form-group">
                <label htmlFor="">Pilih Angkatan</label>
                <select
                  name=""
                  id=""
                  className="form-control"
                  value={angkatan}
                  onChange={(e) => setAngkatan(Number(e.target.value))}
                >
                  {Array.apply(null, Array(5)).map((x, i) => (
                    <option value={i + 2015}>{i + 2015}</option>
                  ))}
                  <option value={-1}>Semua</option>
                </select>
              </div>
            </Col>
            <Col md="2">
              <div className="form-group">
                <label htmlFor="">Pilih Semester Awal</label>
                <select
                  name=""
                  id=""
                  className="form-control"
                  value={smtStart}
                  onChange={(e) => setSmtStart(Number(e.target.value))}
                >
                  {Array.apply(null, Array(10)).map((x, i) => (
                    <option value={i + 106}>{i + 106}</option>
                  ))}
                </select>
              </div>
            </Col>
            <Col md="2">
              <div className="form-group">
                <label htmlFor="">Pilih Semester Akhir</label>
                <select
                  name=""
                  id=""
                  className="form-control"
                  value={smtEnd}
                  onChange={(e) => setSmtEnd(Number(e.target.value))}
                >
                  {Array.apply(null, Array(10)).map((x, i) => (
                    <option value={i + smtStart}>{i + smtStart}</option>
                  ))}
                </select>
              </div>
            </Col>
          </Row>
        </CardHeader>
        <CardBody>
          <Row>
            <Col>
              <DefaultColumnChart
                chartExtraOptions={{
                  plotOptions: {
                    series: {
                      cursor: "pointer",
                      point: {
                        events: {
                          click(e) {
                            setSelectedFakultas(this.series.name);
                          },
                        },
                      },
                    },
                  },
                }}
                semester={Array.apply(null, Array(smtEnd - smtStart + 1)).map(
                  (x, i) => smtStart + i
                )}
                series={randomDataFakultas(ListFakultas)}
                chartTitle={`Grafik rata-rata IPK Per Fakultas`}
                chartSubtitle={`Semester ${smtStart}-${smtEnd}`}
              />
            </Col>
          </Row>
        </CardBody>
      </Card>
      <Card>
        <CardHeader>
          <Row>
            <Col>
              <h4>IPK Rata Rata per Prodi</h4>
            </Col>
            <Col md="2">
              <div className="form-group">
                <label htmlFor="">Pilih Fakultas</label>
                <select
                  name=""
                  id=""
                  className="form-control"
                  value={selectedFakultas}
                  onChange={(e) => setSelectedFakultas(e.target.value)}
                >
                  {ListFakultas.map((x) => (
                    <option value={x.fak}>{x.fak}</option>
                  ))}
                  <option value={"null"} disabled>
                    Pilih atau klik salah satu fakultas
                  </option>
                </select>
              </div>
            </Col>
          </Row>
        </CardHeader>
        <CardBody>
          <Row>
            <Col md="12">
              {selectedFakultas !== "null" && (
                <DefaultColumnChart
                  chartExtraOptions={{}}
                  semester={Array.apply(null, Array(smtEnd - smtStart + 1)).map(
                    (x, i) => smtStart + i
                  )}
                  series={randomDataProdi()}
                  chartTitle={`Grafik rata-rata IPK ${selectedFakultas}`}
                  chartSubtitle={`Semester ${smtStart}-${smtEnd}`}
                />
              )}
            </Col>
          </Row>
        </CardBody>
      </Card>
      <Card>
        <CardHeader>
          <Row>
            <Col>
              <h4>IPK Rata Rata Universitas</h4>
            </Col>
          </Row>
        </CardHeader>
        <CardBody>
          <Row>
            <Col md="12">
              <DefaultColumnChart
                chartExtraOptions={{}}
                semester={Array.apply(null, Array(smtEnd - smtStart + 1)).map(
                  (x, i) => smtStart + i
                )}
                series={randomDataUniversitas()}
                chartTitle={`Grafik rata-rata IPK Universitas`}
                chartSubtitle={`Semester ${smtStart}-${smtEnd}`}
              />
            </Col>
          </Row>
        </CardBody>
      </Card>
    </div>
  );
}
