import React, { useState, useEffect } from "react";
import { Row, Col, Card, CardHeader, CardBody } from "reactstrap";
import DefaultColumnChart from "../../containers/Chart/DefaultColumnChart";
import DefaultPieChart from "../../containers/Chart/DefaultPieChart";

const ListProdi = ["P. TIK", "P. Elektronika", "P. Elektro", "P. Tata Boga"];
const ListFakultas = [
  {
    fak: "Fakultas Ilmu Pendidikan",
    prod: [
      "Bimbingan Dan Konseling",
      "Manajeman Pendidikan",
      "Pendidikan Guru PAUD",
      "Pendidikan Guru Sekolah Dasar",
      "Pendidikan Luar Biasa",
      "Pendidikan Luar Sekolah",
      "Teknologi Pendidikan",
    ],
  },
  {
    fak: "Fakultas Ilmu Sosial",
    prod: [
      "Ilmu Agama Islam",
      "Pendidikan Geografi",
      "Pendidikan IPS",
      "PPKN",
      "Pendidikan Sejarah",
      "Pendidikan Sosiologi",
      "Sosiologi Ilmu Komunikasi",
    ],
  },
  {
    fak: "Fakultas Teknik",
    prod: [
      "Pendidikan Kesejahteraan Keluarga",
      "Pendidikan Tata Boga",
      "Pendidikan Tata Busana",
      "Pendidikan Tata Rias",
      "Pendidikan Teknik Bangunan",
      "Pendidikan Teknik Elektro",
      "Pendidikan Teknik Elektronika",
      "Pendidikan Teknik Informatika dan Komputer",
      "Pendidikan Teknik Mesin",
      "Rekayasa Keselamatan Kebakaran Teknik Mesin",
    ],
  },
  {
    fak: "Fakultas MIPA",
    prod: [
      "Ilmu Komputer",
      "Kimia",
      "Pendidikan Matematika",
      "Pendidikan Fisika",
      "Pendidikan Biologi",
      "Pendidikan Kimia",
    ],
  },
];

const randomDataFakultas = () =>
  ListFakultas.map((x) => {
    return {
      name: x.fak,
      drilldown: x.fak,
      y: parseInt(Math.random() * 1000),
    };
  });
const drillDownFakultas = () => {
  return {
    series: ListFakultas.map((fak) => {
      return {
        id: fak.fak,
        name: fak.fak,
        data: fak.prod.map((x) => [x, parseInt(Math.random() * 150)]),
      };
    }),
  };
};
const randomDataProdi = () =>
  ListProdi.map((x) => {
    return {
      name: x,
      y: parseInt(Math.random() * 120),
    };
  });
const randomPieDataProdi = () =>
  ["Aktif", "Mangkir", "Cuti", "Lulus"].map((x) => ({
    name: x,
    y:
      x == "Mangkir" || x == "Cuti"
        ? parseInt(Math.random() * 20)
        : parseInt(Math.random() * 120),
  }));
const randomPieDataFakultas = () =>
  ["Aktif", "Mangkir", "Cuti", "Lulus"].map((x) => ({
    name: x,
    y:
      x == "Mangkir" || x == "Cuti"
        ? parseInt(Math.random() * 20)
        : parseInt(Math.random() * 120),
  }));
export default function JumlahMahasiswa() {
  const [smtStart, setSmtStart] = useState(106);
  const [angkatanStart, setAngkatanStart] = useState(2016);
  const [fakProdi, setFakProdi] = useState(-1);
  return (
    <div className="animated fadeIn">
      <Card>
        <CardHeader>
          <Row>
            <Col>
              <h4>Jumlah Mahasiswa (Keseluruhan) - Semester {smtStart}</h4>
            </Col>
            <Col md="2">
              <div className="form-group">
                <label htmlFor="">Pilih Angkatan</label>
                <select
                  name=""
                  id=""
                  className="form-control"
                  value={angkatanStart}
                  onChange={(e) => setAngkatanStart(Number(e.target.value))}
                >
                  {Array.apply(null, Array(10)).map((x, i) => (
                    <option value={i + 2016}>{i + 2016}</option>
                  ))}
                </select>
              </div>
            </Col>
            <Col md="2">
              <div className="form-group">
                <label htmlFor="">Pilih Semester</label>
                <select
                  name=""
                  id=""
                  className="form-control"
                  value={smtStart}
                  onChange={(e) => setSmtStart(Number(e.target.value))}
                >
                  {Array.apply(null, Array(10)).map((x, i) => (
                    <option value={i + 106}>{i + 106}</option>
                  ))}
                </select>
              </div>
            </Col>
          </Row>
        </CardHeader>
        <CardBody>
          <Row>
            <Col md="4">
              <DefaultPieChart
                chartTitle="Aktif"
                chartSubtitle={null}
                data={randomDataFakultas()}
                drilldown={drillDownFakultas()}
              />
            </Col>
            <Col md="4">
              <DefaultPieChart
                chartTitle="Mangkir"
                chartSubtitle={null}
                data={randomDataFakultas()}
                drilldown={drillDownFakultas()}
              />
            </Col>
            <Col md="4">
              <DefaultPieChart
                chartTitle="Non Aktif (Cuti)"
                chartSubtitle={null}
                data={randomDataFakultas()}
                drilldown={drillDownFakultas()}
              />
            </Col>
          </Row>
          <Row>
            <Col md="4">
              <DefaultPieChart
                chartTitle="Lulus"
                chartSubtitle={null}
                data={randomDataFakultas()}
                drilldown={drillDownFakultas()}
              />
            </Col>
          </Row>
        </CardBody>
      </Card>
      <Card>
        <CardHeader>
          <Row>
            <Col>
              <h4>Jumlah Mahasiswa (Per Fakultas) - Semester {smtStart}</h4>
            </Col>
          </Row>
        </CardHeader>
        <CardBody>
          <Row>
            {ListFakultas.map((x) => {
              return (
                <Col md="4">
                  <DefaultPieChart
                    chartTitle={x.fak}
                    chartSubtitle={null}
                    data={randomPieDataProdi()}
                  />
                </Col>
              );
            })}
          </Row>
        </CardBody>
      </Card>
      <Card>
        <CardHeader>
          <Row>
            <Col>
              <h4>Jumlah Mahasiswa (Per Prodi) - Semester {smtStart}</h4>
            </Col>
            <Col md="2">
              <div className="form-group">
                <label htmlFor="">Pilih Fakultas</label>
                <select
                  name=""
                  id=""
                  className="form-control"
                  value={fakProdi}
                  onChange={(e) => setFakProdi(Number(e.target.value))}
                >
                  <option value={-1} disabled>
                    Pilih Salah Satu
                  </option>
                  {ListFakultas.map((x, i) => (
                    <option value={i}>{x.fak}</option>
                  ))}
                </select>
              </div>
            </Col>
          </Row>
        </CardHeader>
        <CardBody>
          <Row>
            {fakProdi >= 0 &&
              ListFakultas[fakProdi].prod.map((x) => (
                <Col md="4">
                  <DefaultPieChart
                    chartTitle={x}
                    chartSubtitle={null}
                    data={randomPieDataFakultas()}
                  />
                </Col>
              ))}
          </Row>
        </CardBody>
      </Card>
    </div>
  );
}
